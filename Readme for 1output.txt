1output Version 1.0 19/07/2018

GENERAL USAGE NOTES
--------------------

1.Be aware that the program uses modules:

 urllib.parse - This module defines a standard interface to break Uniform Resource Locator (URL)
strings up in components (addressing scheme, network location, path etc.), to combine 
the components back into a URL string, and to convert a �relative URL� to an absolute URL 
given a �base URL.�
 requests  -  module which makes HTTP requests simpler and more human-friendly. 
 json - module for working with JSON data in Python.

2.Determine the parameters that will be sent to the server.

3.Call method urlencode() for transcoding parameters.

4.Declare "Content-type" and  "Accept" in the header.


5.Call method post() from requests.

6.Call status_code for receiving response code.


-----------------------------------------------------------------------------------------------

Installing does not require special operating system requirements
-------------------------------------------------------------------

Output1 can be reached at:

E-mail: kurylo_elena@meta.ua


Copyright 2018 Elena Kurylo. All rights reserved.
Output1 and its use are subject to a license agreement and are
also subject to copyright, trademark, patent and/or other laws.