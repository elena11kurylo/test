# Verify�correctness�of�returned�response of�method GET for receiving 
a�list�of�all�candidates,Returns�200

import http.client
conn = http.client.HTTPConnection("http://qainterview.cogniance.com/")
conn.request("GET", "/candidates")
r1 = conn.getresponse()
print(r1.status)

data1 = r1.read()
print(data1)
conn.close()


# Verify�correctness�of�returned�response of�method GET that shows�a�candidate�
with�id=<777>,Returns�200

import http.client
conn = http.client.HTTPConnection("http://qainterview.cogniance.com/")
conn.request("GET", "/candidates/777")
r2 = conn.getresponse()
print(r2.status)
conn.close()


#Verify�correctness�of�returned�response of�method POST when�header�Content�Type:�application/json

import urllib.parse
import requests 
import json

f = {'name':'user', 'position':'QA'}
params = urllib.parse.urlencode(f)

headers = {"Content-type": application/json",
           "Accept": "text/plain"}
r = requests.post('http://qainterview.cogniance.com/candidates', data=json.dumps(params), headers=headers)
r.status_code


#Verify�correctness�of�returned�response of�method POST when�name�is�absent

import urllib.parse
import requests 
import json

f = {'position':'QA'}
params = urllib.parse.urlencode(f)

headers = {"Content-type": "text/plain",
           "Accept": "text/plain"}
r = requests.post('http://qainterview.cogniance.com/candidates', data=json.dumps(params), headers=headers)
r.status_code


# Verify�correctness�of�returned�response of�method DELETE that deletes�a candidate�with�id=1535,Returns�200

import http.client
conn = http.client.HTTPConnection("http://qainterview.cogniance.com/")
conn.request("DELETE", "/candidates/1535")
r3 = conn.getresponse()
print(r3.status)

data3 = r3.read()
print(data3)
conn.close()

