2output Version 1.0 19/07/2018

GENERAL USAGE NOTES
--------------------
#1 "Verify�correctness�of�returned�response of�method GET for receiving 
    a�list�of�all�candidates,Returns�200"

1.Be aware that the program uses modules:

http.client - 	this module defines classes which implement the client side of 
		the HTTP and HTTPS protocols.

2.Create An HTTPConnection instance that represents one transaction with an HTTP server.

3.Call method request() with arguments:"GET", "/candidates".

4.Call method getresponse() and printout responses'status.

5.Read the response and save to variable data1.

6.Printout response.

7.Exit from the program.



#2 "Verify�correctness�of�returned�response of�method GET that shows�a�candidate�
    with�id=<777>,Returns�200"

1.Be aware that the program uses modules:

http.client - 	this module defines classes which implement the client side of 
		the HTTP and HTTPS protocols.

2.Create An HTTPConnection instance that represents one transaction with an HTTP server.

3.Call method request() with arguments:"GET", "/candidates/777".

4.Call method getresponse() and printout responses'status.

5.Read the response and save to variable data2.

6.Printout response.

7.Exit from the program.



#3 "Verify�correctness�of�returned�response of�method POST when�header�Content�Type:�application/json"

1.Be aware that the program uses modules:

 urllib.parse - This module defines a standard interface to break Uniform Resource Locator (URL)
		strings up in components (addressing scheme, network location, path etc.), to combine 
		the components back into a URL string, and to convert a �relative URL� to an absolute URL 
		given a �base URL.�
 requests  -  	module which makes HTTP requests simpler and more human-friendly. 
 json - 	module for working with JSON data in Python.

2.Determine the parameters that will be sent to the server.

3.Call method urlencode() for transcoding parameters.

4.Declare "Content-type" as "application/json" and  "Accept" in the header.


5.Call method post() from requests module.

6.Call status_code for receiving response code.



#4 "Verify�correctness�of�returned�response of�method POST when�name�is�absent"

1.Be aware that the program uses modules:

 urllib.parse - This module defines a standard interface to break Uniform Resource Locator (URL)
		strings up in components (addressing scheme, network location, path etc.), to combine 
		the components back into a URL string, and to convert a �relative URL� to an absolute URL 
		given a �base URL.�
 requests  -  	module which makes HTTP requests simpler and more human-friendly. 
 json - 	module for working with JSON data in Python.

2.Determine the parameter'position':'QA' that will be sent to the server.

3.Call method urlencode() for transcoding parameters.

4.Declare "Content-type" and  "Accept" in the header.

5.Call method post() from requests module.

6.Call status_code for receiving response code.



#5 "Verify�correctness�of�returned�response of�method DELETE that deletes�
    a candidate�with�id=1535,Returns�200"

1.Be aware that the program uses modules:

http.client - 	this module defines classes which implement the client side of 
		the HTTP and HTTPS protocols.

2.Create An HTTPConnection instance that represents one transaction with an HTTP server.

3.Call method request() with arguments:"DELETE", "/candidates/1535".

4.Call method getresponse() and printout responses'status.

5.Read the response and save to variable data3.

6.Printout response.

7.Exit from the program.

-----------------------------------------------------------------------------------------------

Installing does not require special operating system requirements
-------------------------------------------------------------------

2output can be reached at:

E-mail: kurylo_elena@meta.ua


Copyright 2018 Elena Kurylo. All rights reserved.
Output1 and its use are subject to a license agreement and are
also subject to copyright, trademark, patent and/or other laws.