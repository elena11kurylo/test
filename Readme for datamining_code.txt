datamining_code Version 1.0 19/07/2018
Java

GENERAL USAGE NOTES
--------------------

1.Be aware that the program uses libraries:

java.io - this library contains the definition of the class PrintWriter and other classes of file input/output.

java.util - this library contains the definition of the class StringTokenizer.

2.Determine the class LogOutput.

3.Declare program entry point.

4.Declare that input and output streams are null.

5.Open try block.

6.Create new instance of the class BufferedReader which causes constructor FileReader of  the class FileReader
  with an argument - name file.txt.
  It wiil be input stream.

7.Initialize line as null.

8.Call method readLine, which line-by-line reads the file.

9.Create new instance of the class StringTokenizer. This constructor has argument - line.
  It will break the lines into tokens.

10.Call constructor StringTokenizer again and search token - "2014:12".

11.Call constructor hasMoreTokens(), which checks if there are more tokens "2014:12".

12.If it is true, then call constructor StringTokenizer again and search token - "200".
 
13.Use count++ in order to count how many times constructor hasMoreTokens()comes back true for token "200" 

14.Determine utputStream through count and save it into file- resultdatamining.txt using constructor 
   FileOutputStream of the class FileReader with an argument - name file.txt.
   It wiil be output stream.

15.Catch exceptions of class FileNotFoundException and class IOException.

16.Program exit.
-----------------------------------------------------------------------------------------------

Installing does not require special operating system requirements
-------------------------------------------------------------------

datamining_code can be reached at:

E-mail: kurylo_elena@meta.ua


Copyright 2018 Elena Kurylo. All rights reserved.
Output1 and its use are subject to a license agreement and are
also subject to copyright, trademark, patent and/or other laws.